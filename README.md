# caddy-s3-proxy

A CICD powered Docker image build for
[caddy-s3-proxy](https://github.com/lindenlab/caddy-s3-proxy).

* Alpine based
* [Docker Hub](https://hub.docker.com/r/bibliosansfrontieres/caddy-s3-proxy):
  `bibliosansfrontieres/caddy-s3-proxy`
* [GitLab registry](https://gitlab.com/bibliosansfrontieres/docker/caddy-s3-proxy/caddy-s3-proxy):
  `bibliosansfrontieres/docker/caddy-s3-proxy/caddy-s3-proxy`

## Configuration

This example configuration is an excerpt from the
[Temps Modernes](https://gitlab.com/bibliosansfrontieres/tm) stack.

### Caddyfile

```text
{
  order s3proxy last
  debug
}

:80 {
  route /olip-packages-prod/* {
      uri strip_prefix olip-packages-prod
      s3proxy /* {
        region "eu-west-1"
        bucket "olip-packages-prod-eu-west-1-481031604408"
        endpoint "s3.eu-west-1.amazonaws.com"
        profile "aws"
      }
    }
  route /olip-catalog-descriptor/* {
      uri strip_prefix olip-catalog-descriptor
      s3proxy /* {
        region "eu-central-1"
        bucket "olip-catalog-descriptor"
        endpoint "s3.eu-central-1.wasabisys.com"
        profile "wasabi"
      }
    }
  log
}
```

Read more about the `Caddyfile` configuration
at [upstream](https://github.com/lindenlab/caddy-s3-proxy#configuration).

### docker-compose

```yaml
  caddy:
    image: bibliosansfrontieres/caddy-s3-proxy
    restart: unless-stopped
    ports:
     - "9998:80"
    labels:
      - "traefik.frontend.rule=Host:s3.eu-central-1.wasabisys.com,download.kiwix.org"
      - "traefik.enable=true"
    volumes:
      - /mnt/s3_conf/madrid/Caddyfile-with-s3-proxy:/Caddyfile
      - /mnt/s3_conf/madrid/aws_profile:/root/.aws/credentials
      - caddy_data:/data
      - caddy_config:/config
    networks:
      - isolated_nw
      - olipcore
    command: ["caddy", "run", "--config", "/Caddyfile"]
```
